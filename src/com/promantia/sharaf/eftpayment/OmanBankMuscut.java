package com.promantia.sharaf.eftpayment;

import java.math.BigDecimal;
import java.text.DecimalFormat;
import java.util.Calendar;
import java.util.logging.Logger;

import javax.swing.JComponent;

import org.codehaus.jettison.json.JSONObject;

import com.openbravo.pos.payment.DevicePayment;
import com.openbravo.pos.payment.PaymentRequest;
import com.openbravo.pos.payment.PaymentResult;
import com.openbravo.poshw.AppConfig;

public class OmanBankMuscut implements DevicePayment {

  public static final int POS_SALES_TYPE = 0;
  public static final int POS_VOID_TYPE = 2;
  public static final int POS_EPP_TYPE = 8;
  public static final int POS_REFUND_TYPE = 1;
  public static final String TRANS_TYPE = "1";
  public static final String TRANS_REFUND_TYPE = "2";
  public static final String TRANS_EPP_TYPE = "8";
  public static final String RESULT_CODE_SUCCESS = "0";
  public static final String OmanBankMuscut_RESPONSE_SUCCESS = "00";
  public static final String RESULT_CODE_FAILURE = "1";

  public static Logger log = Logger.getLogger(OmanBankMuscut.class.getName());

  @Override
  public PaymentResult execute(PaymentRequest params) {
    log.fine("Inside OmanBankMuscutDevice class");

    PaymentResult paymentResult;
    paymentResult = new PaymentResult();
    try {
      String[] args = {};
      AppConfig hwConfig = new AppConfig(args);
      hwConfig.load();

      String parameter = hwConfig.getProperty("machine.payment");

      // To check the mode - test mode or actual
      String mode = "";
      final String[] strParams = parameter.split(":");
      mode = strParams[1];
      log.fine("mode is " + mode);

      log.fine("Parameters from POS " + params.toJSON());

      // multiply the amount by 1000 as a part of OmanBankMuscutDevice device input
      String amount = new DecimalFormat("#")
          .format(params.getAmount().multiply(new BigDecimal(1000))).toString();
      log.fine("amount is " + amount);

      if (mode.equalsIgnoreCase("test")) {
        log.fine("---in testing mode----");
        return executeStub(params, paymentResult, amount);
      }

      // According to POS: if sales, type is 0
      if (params.getType() == POS_SALES_TYPE || params.getType() == POS_EPP_TYPE) {
        paymentResult = executeSalesTransaction(params, amount);
      } else if (params.getType() == POS_VOID_TYPE) {
        paymentResult = executeVoidTransaction(params, amount);
      } else if (params.getType() == POS_REFUND_TYPE) {
        paymentResult = executeRefundTransaction(params, amount);
      }

    } catch (Exception e) {
      log.severe("Error during OmanBankMuscut Payment " + e.getMessage());
      paymentResult.setResultCode(RESULT_CODE_FAILURE);
      paymentResult.setResultMessage(e.getMessage());
    }

    return paymentResult;
  }

  private PaymentResult executeVoidTransaction(PaymentRequest params, String amount)
      throws Exception {

    PaymentResult paymentResult = new PaymentResult();
    paymentResult.setRequest(params);

    bm_wrapper.TxnInput objInput = new bm_wrapper.TxnInput();
    bm_wrapper.TxnOutput voidObjOutput = new bm_wrapper.TxnOutput();
    log.fine("[OmanBankMuscut Void Transaction] ECR Receipt No: "
        + params.getProperties().getString("ecrReceiptNumber") + " Void Receipt No: "
        + params.getProperties().getString("sequenceNumber") + " Terminal ID: "
        + params.getTerminalID() + " Amount: " + amount);

    objInput.VFI_ECRRcptNum = params.getProperties().getString("ecrReceiptNumber");
    objInput.VFI_VoidInvoiceNo = params.getProperties().getString("sequenceNumber");
    objInput.VFI_Amount = amount;

    boolean voidTrans = false;

    voidTrans = bm_wrapper.DLLLoader.VFI_VoidTrans(objInput, voidObjOutput);
    log.fine("VoidTrans retuned: " + voidTrans);
    log.fine(
        "[OmanBankMuscut Void Response] card No: " + voidObjOutput.VFI_CardNum + " Approval code: "
            + voidObjOutput.VFI_ApprovalCode + " Sequence no: " + voidObjOutput.VFI_MessNum
            + " ECR Receipt no: " + voidObjOutput.VFI_ECRRcptNum + " Result Code: "
            + voidObjOutput.VFI_Respcode + " Result Message: " + voidObjOutput.VFI_RespMess);

    // to send the result data back to pos
    JSONObject voidResultObj = new JSONObject();
    if (voidObjOutput.VFI_Respcode.isEmpty() || "".equals(voidObjOutput.VFI_Respcode)
        || !OmanBankMuscut_RESPONSE_SUCCESS.equals(voidObjOutput.VFI_Respcode)) {
      paymentResult.setResultCode(voidObjOutput.VFI_Respcode);
      paymentResult.setResultMessage("Failure");
    } else {
      voidResultObj.put("cardNumber", voidObjOutput.VFI_CardNum);
      voidResultObj.put("approvalCode", voidObjOutput.VFI_ApprovalCode);
      voidResultObj.put("sequenceNumber", voidObjOutput.VFI_MessNum);
      voidResultObj.put("ecrReceiptNumber", voidObjOutput.VFI_ECRRcptNum);
      voidResultObj.put("amount", objInput.VFI_Amount);
      paymentResult.setResultCode(RESULT_CODE_SUCCESS);
      paymentResult.setResultMessage(voidObjOutput.VFI_RespMess);
      paymentResult.setProperties(voidResultObj);

    }
    return paymentResult;
  }

  private PaymentResult executeRefundTransaction(PaymentRequest params, String amount)
      throws Exception {

    PaymentResult paymentResult = new PaymentResult();

    paymentResult.setRequest(params);
    bm_wrapper.TxnInput objInput = new bm_wrapper.TxnInput();
    bm_wrapper.TxnOutput objOutput = new bm_wrapper.TxnOutput();

    // Prepare inputs for request
    Calendar calendar = Calendar.getInstance();
    String uniqueNo = "";
    uniqueNo = String.valueOf(calendar.get(Calendar.HOUR))
        + String.valueOf(calendar.get(Calendar.MINUTE))
        + String.valueOf(calendar.get(Calendar.SECOND));

    // log input parameters
    log.fine("[OmanBankMuscut Sales Request] Terminal ID: " + params.getTerminalID() + " amount: "
        + amount + " ECR Rceipt NO: " + uniqueNo + "Type: " + TRANS_TYPE);

    // set input parameters
    objInput.VFI_Amount = amount;
    objInput.VFI_ECRRcptNum = uniqueNo;
    // objInput.VFI_TID = params.getTerminalID();
    // objInput.VFI_TransType = TRANS_REFUND_TYPE;
    // objInput.VFI_ApprovalCode = params.getProperties().getString("originalApprovalCode");
    // objInput.VFI_CardNum = params.getProperties().getString("originalCreditCardNum");

    // call get auth method
    boolean response = false;
    response = bm_wrapper.DLLLoader.VFI_GetAuth(objInput, objOutput);

    log.fine("GetAuth retuned: " + response);
    log.fine("[OmanBankMuscut Sales Response] cardlogo: " + objOutput.VFI_CardSchemeName
        + " Card Number: " + objOutput.VFI_CardNum + " Approval code " + objOutput.VFI_ApprovalCode
        + " Sequence no: " + objOutput.VFI_MessNum + " ECR receipt no: " + objOutput.VFI_ECRRcptNum
        + " Result code: " + objOutput.VFI_Respcode + " Response message: "
        + objOutput.VFI_RespMess);

    // to send the result data back to pos
    JSONObject resultObj = new JSONObject();
    if (objOutput.VFI_Respcode.isEmpty() || "".equals(objOutput.VFI_Respcode)
        || !OmanBankMuscut_RESPONSE_SUCCESS.equals(objOutput.VFI_Respcode)) {
      paymentResult.setResultCode(objOutput.VFI_Respcode);
      paymentResult.setResultMessage("Failure");
    } else {
      resultObj.put("cardlogo", objOutput.VFI_CardSchemeName);
      resultObj.put("cardNumber", objOutput.VFI_CardNum);
      resultObj.put("approvalCode", objOutput.VFI_ApprovalCode);
      resultObj.put("sequenceNumber", objOutput.VFI_MessNum);
      resultObj.put("ecrReceiptNumber", objOutput.VFI_ECRRcptNum);
      paymentResult.setResultCode(RESULT_CODE_SUCCESS);
      paymentResult.setResultMessage(objOutput.VFI_RespMess);
      paymentResult.setProperties(resultObj);

    }

    return paymentResult;
  }

  private PaymentResult executeSalesTransaction(PaymentRequest params, String amount)
      throws Exception {

    PaymentResult paymentResult = new PaymentResult();

    paymentResult.setRequest(params);
    bm_wrapper.TxnInput objInput = new bm_wrapper.TxnInput();
    bm_wrapper.TxnOutput objOutput = new bm_wrapper.TxnOutput();

    // Prepare inputs for request
    Calendar calendar = Calendar.getInstance();
    String uniqueNo = "";
    uniqueNo = String.valueOf(calendar.get(Calendar.HOUR))
        + String.valueOf(calendar.get(Calendar.MINUTE))
        + String.valueOf(calendar.get(Calendar.SECOND));

    // log input parameters
    log.fine("[OmanBankMuscut Sales Request] Terminal ID: " + params.getTerminalID() + " amount: "
        + amount + " ECR Rceipt NO: " + uniqueNo + "Type: " + TRANS_TYPE);

    // set input parameters
    objInput.VFI_Amount = amount;
    objInput.VFI_ECRRcptNum = uniqueNo;

    // call get auth method
    boolean response = false;
    response = bm_wrapper.DLLLoader.VFI_GetAuth(objInput, objOutput);

    log.fine("GetAuth retuned: " + response);
    log.fine("[OmanBankMuscut Sales Response] cardlogo: " + objOutput.VFI_CardSchemeName
        + " Card Number: " + objOutput.VFI_CardNum + " Approval code " + objOutput.VFI_ApprovalCode
        + " Sequence no: " + objOutput.VFI_MessNum + " ECR receipt no: " + objOutput.VFI_ECRRcptNum
        + " Result code: " + objOutput.VFI_Respcode + " Response message: "
        + objOutput.VFI_RespMess);

    // to send the result data back to pos
    JSONObject resultObj = new JSONObject();
    if (objOutput.VFI_Respcode.isEmpty() || "".equals(objOutput.VFI_Respcode)
        || !OmanBankMuscut_RESPONSE_SUCCESS.equals(objOutput.VFI_Respcode)) {
      paymentResult.setResultCode(objOutput.VFI_Respcode);
      paymentResult.setResultMessage("Failure");
    } else {
      resultObj.put("cardlogo", objOutput.VFI_CardSchemeName);
      resultObj.put("cardNumber", objOutput.VFI_CardNum);
      resultObj.put("approvalCode", objOutput.VFI_ApprovalCode);
      resultObj.put("sequenceNumber", objOutput.VFI_MessNum);
      resultObj.put("ecrReceiptNumber", objOutput.VFI_ECRRcptNum);
      paymentResult.setResultCode(RESULT_CODE_SUCCESS);
      paymentResult.setResultMessage(objOutput.VFI_RespMess);
      paymentResult.setProperties(resultObj);

    }

    return paymentResult;
  }

  private PaymentResult executeStub(PaymentRequest params, PaymentResult pr, String amount) {

    JSONObject resultObj = new JSONObject();
    // According to POS: if sales, type is 0
    try {
      if (params.getType() == 0 || params.getType() == 8 || params.getType() == 1) {

        pr.setRequest(params);

        resultObj.put("cardNumber", "1234****7890");
        resultObj.put("approvalCode", "1234");
        resultObj.put("sequenceNumber", "26");
        resultObj.put("ecrReceiptNumber", "0001");
        resultObj.put("cardlogo", "MASTER");

        pr.setProperties(resultObj);
        pr.setResultCode(RESULT_CODE_SUCCESS);
        pr.setResultMessage("Transaction successful");
        log.fine("successful payment transaction");
        log.fine("amount" + amount);
        // According to POS: if void, type is 2
      } else if (params.getType() == 2) {
        log.fine("Inside void process method -stub code");
        log.fine("original transaction properties are " + params.getProperties().toString());

        resultObj.put("cardNumber", "1234****7890");
        resultObj.put("approvalCode", "1234");
        resultObj.put("amount", amount);

        pr.setProperties(resultObj);
        pr.setResultCode(RESULT_CODE_SUCCESS);
        pr.setResultMessage("Transaction successful");
        log.fine("successful void process");
      }
    } catch (Exception e) {
      log.severe("Error during OmanBankMuscut Payment test flow " + e.getMessage());
      pr.setResultCode(RESULT_CODE_FAILURE);
      pr.setResultMessage(e.getMessage());
    }

    return pr;

  }

  @Override
  public JComponent getComponent() {
    return null;
  }

  @Override
  public String getDescription() {
    return "OmanBankMuscut Payment Integration";
  }

  @Override
  public String getName() {
    return "OmanBankMuscut Payment Device";
  }
}
