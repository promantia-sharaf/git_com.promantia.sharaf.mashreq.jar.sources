package com.promantia.sharaf.eftpayment;

import java.util.logging.Logger;

import com.openbravo.pos.payment.DevicePayment;
import com.openbravo.pos.service.HardwareConfig;
import com.openbravo.pos.service.PaymentService;

public class EFTPaymentService implements PaymentService{

	public static final Logger log = Logger.getLogger(EFTPaymentService.class.getName());
	
	@Override
	public DevicePayment getPayment(String type, HardwareConfig hwconfig)
			throws Exception {
		log.info("Inside EFTPaymentService class" + " parameter type is "+ type);
		DevicePayment device = new EftPaymentDevice();
		return device;
	}

}
