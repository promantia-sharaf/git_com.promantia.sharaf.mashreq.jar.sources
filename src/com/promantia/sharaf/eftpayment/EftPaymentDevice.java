package com.promantia.sharaf.eftpayment;

import java.util.logging.Logger;

import javax.swing.JComponent;

import org.codehaus.jettison.json.JSONException;

import com.openbravo.pos.payment.DevicePayment;
import com.openbravo.pos.payment.PaymentRequest;
import com.openbravo.pos.payment.PaymentResult;

public class EftPaymentDevice implements DevicePayment {
  public static final String mashreq = "integrated.mashreq";
  public static final String credimax = "integrated.credimax";
  public static final String eazypay = "integrated.eazypay";
  public static final String bankMuscat = "integrated.bankmuscat";
  public static Logger log = Logger.getLogger(EftPaymentDevice.class.getName());

  @Override
  public PaymentResult execute(PaymentRequest params) {
    log.fine("Inside EftPaymentDevice class ");
    PaymentResult result = new PaymentResult();

    try {
      log.fine("Inside EftPaymentDevice class try block" + params.toJSON().toString());
      String providerCode = params.toJSON().getJSONObject("properties").getString("providerCode");

      switch (providerCode) {
      case mashreq:
        log.fine("Inside EftPaymentDevice class Case mashreq" + params.toJSON().toString());
        DevicePayment dp = new MashreqPaymentDevice();
        result = dp.execute(params);
        break;
      case credimax:
        log.fine("Inside EftPaymentDevice class case Credimax " + params.toJSON().toString());
        DevicePayment dpt = new CrediMaxPaymentDevice();
        result = dpt.execute(params);
        break;
      case eazypay:
        log.fine("Inside EftPaymentDevice class case Eazypay " + params.toJSON().toString());
        DevicePayment devpay = new EazyPaymentDevice();
        result = devpay.execute(params);
        break;
      case bankMuscat:
        log.fine("Inside EftPaymentDevice class case BankMuscat " + params.toJSON().toString());
        DevicePayment Bm = new OmanBankMuscut();
        result = Bm.execute(params);
        break;
      }
    } catch (JSONException e) {

    }
    log.fine("End EftPaymentDevice class ");
    return result;
  }

  @Override
  public JComponent getComponent() {
    // TODO Auto-generated method stub
    return null;
  }

  @Override
  public String getDescription() {
    // TODO Auto-generated method stub
    return null;
  }

  @Override
  public String getName() {
    // TODO Auto-generated method stub
    return null;
  }

}
