package com.promantia.sharaf.eftpayment;

import java.io.File;
import java.math.BigDecimal;
import java.text.DecimalFormat;
import java.util.Calendar;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.swing.JComponent;

import org.codehaus.jettison.json.JSONObject;

import com.openbravo.pos.payment.DevicePayment;
import com.openbravo.pos.payment.PaymentRequest;
import com.openbravo.pos.payment.PaymentResult;
import com.openbravo.poshw.AppConfig;

import net.sf.jni4net.Bridge;
import paxdll.java.ePOS;

public class EazyPaymentDevice implements DevicePayment {

  public static final int POS_SALES_TYPE = 0;
  public static final int POS_VOID_TYPE = 2;
  public static final int POS_EPP_TYPE = 8;
  public static final int POS_REFUND_TYPE = 1;
  public static final String TRANS_TYPE = "1";
  public static final String TRANS_REFUND_TYPE = "2";
  public static final String TRANS_EPP_TYPE = "8";
  public static final String RESULT_CODE_SUCCESS = "0";
  public static final String RESULT_CODE_FAILURE = "1";
  public static final String EAZYPAY_RESPONSE_SUCCESS = "1";
  public static final String EAZYPAY_RESPONSE_SUCCESS_MSG = "Success";
  public static boolean isEazypayConfigured = false;

  public static Logger log = Logger.getLogger(EazyPaymentDevice.class.getName());

  @Override
  public PaymentResult execute(PaymentRequest params) {
    log.fine("Inside EazyPaymentDevice class");

    PaymentResult paymentResult;
    paymentResult = new PaymentResult();
    try {

      String[] args = {};
      AppConfig hwConfig = new AppConfig(args);
      hwConfig.load();

      String parameter = hwConfig.getProperty("machine.payment");

      // To check the mode - test mode or actual
      String mode = "";
      final String[] strParams = parameter.split(":");
      mode = strParams[1];
      log.fine("mode is " + mode);

      log.fine("Parameters from POS " + params.toJSON());

      // multiply the amount by 100 as a part of eazy pay device input
      String amount = new DecimalFormat("#")
          .format(params.getAmount().multiply(new BigDecimal(1000))).toString();
      log.fine("amount is " + amount);

      if (mode.equalsIgnoreCase("test")) {
        log.fine("---in testing mode----");
        return executeStub(params, paymentResult, amount);
      }

      // According to POS: if sales, type is 0
      if (params.getType() == POS_SALES_TYPE) {
        paymentResult = executeSalesTransaction(params, amount);
      } else if (params.getType() == POS_VOID_TYPE) {
        paymentResult = executeVoidTransaction(params, amount);
      }

    } catch (Exception e) {
      // log.log(Level.SEVERE, "Error during Eazy Payment", e);
      log.log(Level.FINE, "Error during Eazy Payment", e);
      paymentResult.setResultCode(RESULT_CODE_FAILURE);
      paymentResult.setResultMessage(e.getMessage());
    }

    return paymentResult;
  }

  private PaymentResult executeSalesTransaction(PaymentRequest params, String amount)
      throws Exception {

    String[] args = {};
    AppConfig hwConfig = new AppConfig(args);
    hwConfig.load();
    String dllFile = hwConfig.getProperty("machine.payment.eazypay.dll");

    String txnTimeOut = hwConfig.getProperty("machine.payment.eazypay.txntimeout");
    String ackTimeout = hwConfig.getProperty("machine.payment.eazypay.acktimeout");
    String DataLogPath = hwConfig.getProperty("machine.payment.eazypay.dataLogPath");

    log.fine(" txnTimeOut is " + txnTimeOut + " ackTimeout is " + ackTimeout + "datalogpath"
        + DataLogPath);

    String ezaypayTerminalId = params.toJSON().getJSONObject("properties")
        .getString("EazypayTerminalId");

    boolean Ret;
    PaymentResult paymentResult = new PaymentResult();
    paymentResult.setRequest(params);
    Bridge.setVerbose(true);
    Bridge.init();
    File DllFile = new File(dllFile);
    Bridge.LoadAndRegisterAssemblyFrom(DllFile);
    log.fine("After reading dll file");

    if (!isEazypayConfigured) {
      ePOS.SetVariable(51, ackTimeout);
      ePOS.SetVariable(52, txnTimeOut);
      ePOS.SetVariable(54, DataLogPath);
      ePOS.SetVariable(55, "true");
      ePOS.PAX_SetConfiguration();
      log.fine("After calling setconfiguration method");

      ePOS.PAX_Initialisation();
      log.fine("After calling initialisation method");
      isEazypayConfigured = true;
      try {
        Thread.sleep(1000);
      } catch (Exception e) {
        log.log(Level.FINE, "Error during Eazy Payment initialisation", e);
      }
    }

    Calendar calendar = Calendar.getInstance();
    String uniqueNo = "";
    uniqueNo = String.valueOf(calendar.get(Calendar.HOUR))
        + String.valueOf(calendar.get(Calendar.MINUTE))
        + String.valueOf(calendar.get(Calendar.SECOND));

    ePOS.SetVariable(1, uniqueNo);
    ePOS.SetVariable(2, amount);
    ePOS.SetVariable(44, ezaypayTerminalId);

    log.fine("before calling sale method");

    Ret = ePOS.PAX_Sale();
    // to send the result data back to pos
    JSONObject refundResultObj = new JSONObject();
    if (Ret && EAZYPAY_RESPONSE_SUCCESS.equals(ePOS.GetVariable(10))
        && ePOS.GetVariable(11).contains(EAZYPAY_RESPONSE_SUCCESS_MSG)) {
      refundResultObj.put("cardlogo", ePOS.GetVariable(7));
      refundResultObj.put("cardNumber", ePOS.GetVariable(5));
      refundResultObj.put("approvalCode", ePOS.GetVariable(12));
      refundResultObj.put("ecrReceiptNumber", ePOS.GetVariable(1));
      refundResultObj.put("PosInvoice", ePOS.GetVariable(6));
      refundResultObj.put("PAX_RRN", ePOS.GetVariable(23));
      paymentResult.setResultCode(RESULT_CODE_SUCCESS);
      paymentResult.setResultMessage(ePOS.GetVariable(11));
      paymentResult.setProperties(refundResultObj);

    } else {
      paymentResult.setResultCode(ePOS.GetVariable(10));
      paymentResult.setResultMessage(ePOS.GetVariable(11));
    }
    return paymentResult;
  }

  private PaymentResult executeVoidTransaction(PaymentRequest params, String amount)
      throws Exception {

    String[] args = {};
    AppConfig hwConfig = new AppConfig(args);
    hwConfig.load();
    String dllFile = hwConfig.getProperty("machine.payment.eazypay.dll");

    String txnTimeOut = hwConfig.getProperty("machine.payment.eazypay.txntimeout");
    String ackTimeout = hwConfig.getProperty("machine.payment.eazypay.acktimeout");
    String DataLogPath = hwConfig.getProperty("machine.payment.eazypay.dataLogPath");

    log.fine(" txnTimeOut is " + txnTimeOut + " ackTimeout is " + ackTimeout + "datalogpath"
        + DataLogPath);

    String ezaypayTerminalId = params.toJSON().getJSONObject("properties")
        .getString("EazypayTerminalId");
    String posInvoice = params.toJSON().getJSONObject("properties")
        .getString("posInvoiceForEazypay");
    String paxRRN = params.toJSON().getJSONObject("properties").getString("paxRrn");

    boolean Ret;
    PaymentResult paymentResult = new PaymentResult();
    paymentResult.setRequest(params);
    Bridge.setVerbose(true);
    Bridge.init();
    File DllFile = new File(dllFile);
    Bridge.LoadAndRegisterAssemblyFrom(DllFile);

    if (!isEazypayConfigured) {
      ePOS.SetVariable(51, ackTimeout);
      ePOS.SetVariable(52, txnTimeOut);
      ePOS.SetVariable(54, DataLogPath);
      ePOS.SetVariable(55, "true");
      ePOS.PAX_SetConfiguration();

      ePOS.PAX_Initialisation();
      isEazypayConfigured = true;
      try {
        Thread.sleep(1000);
      } catch (Exception e) {
        log.log(Level.FINE, "Error during Eazy Payment initialisation", e);
      }
    }

    Calendar calendar = Calendar.getInstance();
    String uniqueNo = "";
    uniqueNo = String.valueOf(calendar.get(Calendar.HOUR))
        + String.valueOf(calendar.get(Calendar.MINUTE))
        + String.valueOf(calendar.get(Calendar.SECOND));

    ePOS.SetVariable(1, uniqueNo);
    ePOS.SetVariable(2, amount);
    ePOS.SetVariable(44, ezaypayTerminalId);
    ePOS.SetVariable(6, posInvoice);
    ePOS.SetVariable(23, paxRRN);

    Ret = ePOS.PAX_Void();
    // to send the result data back to pos
    JSONObject refundResultObj = new JSONObject();
    if (Ret && EAZYPAY_RESPONSE_SUCCESS.equals(ePOS.GetVariable(10))
        && ePOS.GetVariable(11).contains(EAZYPAY_RESPONSE_SUCCESS_MSG)) {

      refundResultObj.put("cardNumber", ePOS.GetVariable(5));
      refundResultObj.put("approvalCode", ePOS.GetVariable(12));
      refundResultObj.put("amount", ePOS.GetVariable(2));
      refundResultObj.put("ecrReceiptNumber", ePOS.GetVariable(1));
      paymentResult.setResultCode(RESULT_CODE_SUCCESS);
      paymentResult.setResultMessage(ePOS.GetVariable(11));
      paymentResult.setProperties(refundResultObj);

    } else {
      paymentResult.setResultCode(ePOS.GetVariable(10));
      paymentResult.setResultMessage(ePOS.GetVariable(11));
    }
    return paymentResult;
  }

  private PaymentResult executeStub(PaymentRequest params, PaymentResult pr, String amount) {

    JSONObject resultObj = new JSONObject();
    // According to POS: if sales, type is 0
    try {
      if (params.getType() == 0 || params.getType() == 8 || params.getType() == 1) {

        pr.setRequest(params);

        resultObj.put("cardNumber", "1234****7890");
        resultObj.put("approvalCode", "1234");
        resultObj.put("sequenceNumber", "26");
        resultObj.put("cardlogo", "MASTER");
        resultObj.put("PosInvoice", "3456");
        resultObj.put("PAX_RRN", "1999");

        pr.setProperties(resultObj);
        pr.setResultCode(RESULT_CODE_SUCCESS);
        pr.setResultMessage("Transaction successful");
        log.fine("successful payment transaction");

        // According to POS: if void, type is 2
      } else if (params.getType() == 2) {
        log.fine("Inside void process method -stub code");
        log.fine("original transaction properties are " + params.getProperties().toString());

        resultObj.put("cardNumber", "1234****7890");
        resultObj.put("approvalCode", "1234");
        resultObj.put("amount", amount);

        pr.setProperties(resultObj);
        pr.setResultCode(RESULT_CODE_SUCCESS);
        pr.setResultMessage("Transaction successful");
        log.fine("successful void process");
      }
    } catch (Exception e) {
      log.severe("Error during Eazy Payment test flow " + e.getMessage());
      pr.setResultCode(RESULT_CODE_FAILURE);
      pr.setResultMessage(e.getMessage());
    }

    return pr;

  }

  @Override
  public JComponent getComponent() {
    return null;
  }

  @Override
  public String getDescription() {
    return "Eazy Payment Integration";
  }

  @Override
  public String getName() {
    return "Eazy Payment Device";
  }
}
